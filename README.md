# A Small Personal Blog Project

### Powered by:
[![Django](https://cdn.iconscout.com/icon/free/png-128/django-12-1175186.png)](https://www.djangoproject.com/)

This is a small project with basic blog functionalities as:

  - Registering users;
  - Management of registered user's personal info;
  - Articles publishing for registered users;
  - Management of published articles for registered users;
  - Comments for articles for registered users with edit/delete options;
  - Likes/Dislikes for articles from registered users and corresponding ratings;
  - Public access to view/read all articles;
  - etc.

You can also:
  - Upload some user/article pictures;
  - Collaborate to extend project code with a brand new features.

> This is my first Django web app effort from scratch. It has many flaws in structure and so on,
> but the idea was purely for education purpose.

### Used Tech:

To make this project work properly I've used:

* [Python 3.8.6](https://www.python.org/downloads/release/python-386/) - A popular programming language with a beautiful syntax!
* [Django 3.1](https://www.djangoproject.com/) - A Python-based free and open-source web framework.
* [Twitter Bootstrap](https://getbootstrap.com/) - good enough looking UI css templates for modern web apps.
* [PyCharm Pro 2020.3](https://www.jetbrains.com/pycharm/) - IDE used in computer programming, specifically for the Python language.
* [Lubuntu 20.10](https://lubuntu.me/) - Ubuntu based lightweight Linux distribution.
* [GitLab](https://about.gitlab.com/) - Software FREEDOM!

### Usage:

This blog-project may be uploaded to [Heroku](https://www.heroku.com/) for real usage some day... but not today :)

### Project Dependencies:

| Plugin | Version |
| ------ | ------ |
| asgiref | 3.3.1 |
| Django | 3.1.4 |
| Pillow | 8.0.1 |
| pytz | 2020.4 |
| sqlparse | 0.4.1 |

### Todos:

 - Write some tests;
 - Add new functionalities;
 - Write new interactive UI...

### License:

GNU 3

**FOSS 4 life!**

P.S.:
----

> I am not a frontender! I write code, not designs.
> And I like simple designs.