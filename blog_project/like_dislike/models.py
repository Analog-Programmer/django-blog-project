from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.
from blog_project.articles.models import Articles


def validate_value(value):
    if value not in {-1, 1}:
        raise ValidationError('The value must be 1 or -1!')


class ArticleLikesAndDislikes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    article = models.ForeignKey(Articles, on_delete=models.CASCADE)
    date_liked = models.DateTimeField(auto_now_add=True)

    like = models.IntegerField(editable=False, validators=[validate_value])

    # for admin panel functionality:
    def __str__(self):
        article = self.article.title if len(self.article.title) <= 30 else self.article.title[:30] + '...'
        return f'{self.user.username} likes "{article}"' if self.like == 1 else f'{self.user.username} dislikes "{article}"'
