from django.contrib import admin

# Register your models here.
from blog_project.like_dislike.models import ArticleLikesAndDislikes

admin.site.register(ArticleLikesAndDislikes)
