from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect

# Create your views here.
from django.views.decorators.http import require_GET

from blog_project.articles.models import Articles
from blog_project.like_dislike.models import ArticleLikesAndDislikes

from blog_project.utils.common_view_funcs import check_if_article_exists


@require_GET
@login_required
def like_article(request, pk):
    check_if_article_exists(pk)

    article_to_like = ArticleLikesAndDislikes.objects.filter(article_id=pk)
    if not article_to_like.filter(user_id=request.user.pk).exists():
        ArticleLikesAndDislikes.objects.create(article_id=pk, user_id=request.user.pk, like=1).save()
        article = Articles.objects.get(pk=pk)  # for slug URL redirection
        return redirect('show article', pk, article.slug)

    raise PermissionDenied  # authorisation error


@require_GET
@login_required
def dislike_article(request, pk):
    check_if_article_exists(pk)

    article_to_dislike = ArticleLikesAndDislikes.objects.filter(article_id=pk)
    if not article_to_dislike.filter(user_id=request.user.pk).exists():
        ArticleLikesAndDislikes.objects.create(article_id=pk, user_id=request.user.pk, like=-1).save()
        article = Articles.objects.get(pk=pk)  # for slug URL redirection
        return redirect('show article', pk, article.slug)

    raise PermissionDenied  # authorisation error
