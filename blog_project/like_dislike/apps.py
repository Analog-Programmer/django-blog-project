from django.apps import AppConfig


class LikeDislikeConfig(AppConfig):
    name = 'blog_project.like_dislike'
