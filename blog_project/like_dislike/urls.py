from django.urls import path

from blog_project.like_dislike import views

urlpatterns = [
    path('like/<int:pk>', views.like_article, name='like article'),
    path('dislike/<int:pk>', views.dislike_article, name='dislike article'),
    ]
