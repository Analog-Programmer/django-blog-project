"""blog_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from blog_project.articles import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.list_all_articles, name='index page'),
    path('users/', include('blog_project.user_auth.urls')),
    path('articles/', include('blog_project.articles.urls')),
    path('about/', views.show_about, name='about page'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
