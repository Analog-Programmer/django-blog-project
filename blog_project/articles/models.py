from os.path import join

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils.text import slugify


# creates path & filename for uploaded article picture as "media/images/[article_id]_article_pic_[username]_[filename]":
def make_image_upload_path(instance, filename):
    return join('images', str(instance.id) + '_article_pic_' + instance.user.username + '_' + filename.lower())


class Articles(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    title = models.CharField(max_length=150, blank=False)
    content = models.TextField(blank=False)
    article_pic = models.ImageField(upload_to=make_image_upload_path, blank=True)

    slug = models.SlugField(editable=False)

    # for autogenerated slug:
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    # for admin panel functionality:
    def __str__(self):
        article = self.title if len(self.title) <= 30 else self.title[:30] + '...'
        return f'{self.user.username} published "{article}"'
