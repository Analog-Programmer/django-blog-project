from django.apps import AppConfig


class ArticlesConfig(AppConfig):
    name = 'blog_project.articles'
