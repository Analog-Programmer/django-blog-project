from django.urls import path, include

from blog_project.articles import views

urlpatterns = [
    path('yours/<int:pk>', views.list_your_articles, name='your articles'),
    path('create/', views.create_article, name='create article'),
    path('show/<int:pk>/<slug:slug>', views.show_article, name='show article'),
    path('edit/<int:pk>', views.edit_article, name='edit article'),
    path('confirm_delete/<int:pk>', views.confirm_delete_article, name='confirm delete article'),
    path('delete/<int:pk>', views.delete_article, name='delete article'),
    path('comments/', include('blog_project.article_comments.urls')),
    path('likes/', include('blog_project.like_dislike.urls')),
]
