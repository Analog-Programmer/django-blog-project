from django import forms

from blog_project.articles.models import Articles


# use this to attach attributes to widgets in ModelForms with class mixin:
class StylizeFormMixin(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs.update(
                {
                    'class': 'form-control  form-group',
                }
            )


class ArticleModelForm(StylizeFormMixin, forms.ModelForm):  # attaches attributes to widgets in ModelForms with class mixin
    class Meta:
        model = Articles
        exclude = ['user', 'date_created', 'last_modified']


class ArticleEditModelForm(StylizeFormMixin, forms.ModelForm):  # attaches attributes to widgets in ModelForms with class mixin
    class Meta:
        model = Articles
        fields = ['content', 'article_pic']
