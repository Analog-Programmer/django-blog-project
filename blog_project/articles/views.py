from os import remove, listdir
from os.path import join, isfile, exists

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.http import require_GET

from blog_project.article_comments.forms import CommentModelForm
from blog_project.article_comments.models import ArticleComments
from blog_project.articles.forms import ArticleModelForm, ArticleEditModelForm
from blog_project.articles.models import Articles
from blog_project.like_dislike.models import ArticleLikesAndDislikes
from blog_project.settings import MEDIA_ROOT

from blog_project.utils.common_view_funcs import check_if_user_exists, check_if_article_exists


@require_GET
def list_all_articles(request):
    articles = Articles.objects.all()

    context = {
        'articles': articles.order_by('-date_created'),
    }

    return render(request, 'index.html', context)


@require_GET
@login_required
def list_your_articles(request, pk):
    check_if_user_exists(pk)
    articles = Articles.objects.filter(user_id=pk)

    if request.user.pk != pk:
        raise PermissionDenied  # authorisation error

    context = {
        'articles': articles.order_by('-date_created'),
    }

    return render(request, 'articles/your_articles.html', context)


def show_article(request, pk, slug):
    check_if_article_exists(pk)

    article = Articles.objects.get(pk=pk)
    article_pic = None
    if article.article_pic:
        article_pic = article.article_pic.url

    comments_list = None
    if ArticleComments.objects.filter(article_id=pk).exists():
        comments_list = ArticleComments.objects.filter(article_id=pk)

    other_articles_list = Articles.objects.exclude(pk=pk)

    user_can_like = False
    article_to_like = ArticleLikesAndDislikes.objects.filter(article_id=pk)
    if not article_to_like.filter(user_id=request.user.pk).exists():
        user_can_like = True

    article_rating = 0
    if ArticleLikesAndDislikes.objects.filter(article_id=pk).exists():
        article_likes = ArticleLikesAndDislikes.objects.filter(article_id=pk)
        article_rating = sum(x.like for x in article_likes)

    if request.method == 'GET':

        context = {
            'article': article,
            'article_pic': article_pic,
            'comments_list': comments_list,
            'comment_form': CommentModelForm(),
            'other_articles_list': other_articles_list.order_by('-date_created'),  # ordered by date/time created - newest on top
            'user_can_like': user_can_like,
            'article_rating': article_rating,
        }

        return render(request, 'articles/article_details.html', context)

    else:
        if request.user.is_authenticated:
            comment_form = CommentModelForm(request.POST)

            if comment_form.is_valid():
                comment = comment_form.save(commit=False)
                comment.article_id = pk
                comment.user_id = request.user.id
                comment.save()

                return redirect('show article', pk, slug)

            context = {
                'article': article,
                'article_pic': article_pic,
                'comments_list': comments_list,
                'comment_form': comment_form,
                'other_articles_list': other_articles_list.order_by('-date_created'),  # ordered by date/time created - newest on top
                'user_can_like': user_can_like,
                'article_rating': article_rating,
            }

            return render(request, 'articles/article_details.html', context)

        raise PermissionDenied  # authorisation error


@login_required
def create_article(request):
    if request.method == 'GET':
        context = {
            'form': ArticleModelForm(),
        }

    else:
        form = ArticleModelForm(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.user = request.user
            article.save()

            if 'article_pic' in request.FILES:
                article.article_pic = request.FILES['article_pic']
                article.save()

            return redirect('index page')

        context = {
            'form': form,
        }

    return render(request, 'articles/create_article.html', context)


@login_required
def edit_article(request, pk):
    check_if_article_exists(pk)

    article_to_edit = Articles.objects.get(pk=pk)
    article_pic = None
    if article_to_edit.article_pic:
        article_pic = article_to_edit.article_pic.url

    if request.user == article_to_edit.user:
        if request.method == 'GET':

            context = {
                'form': ArticleEditModelForm(instance=article_to_edit),
                'article': article_to_edit,
                'article_pic': article_pic,
            }

        else:
            form = ArticleEditModelForm(request.POST, instance=article_to_edit)
            if form.is_valid():
                old_pic_file_path = join(MEDIA_ROOT, str(article_to_edit.article_pic))
                article = form.save(commit=False)

                # deletes old article pic if new image file is uploaded:
                if 'article_pic' in request.FILES:
                    if isfile(old_pic_file_path):
                        remove(old_pic_file_path)
                    article.article_pic = request.FILES['article_pic']

                article.save()

                # deletes all old article pics if current pic is not set:
                if not article.article_pic:
                    path_to_files = join(MEDIA_ROOT, 'images')
                    if exists(path_to_files):
                        [remove(join(path_to_files, file)) for file in listdir(path_to_files)
                         if exists(join(path_to_files, file))
                         and str(article.id) + '_article_pic_' + article.user.username + '_'
                         in join(path_to_files, file)]

                return redirect('your articles', article_to_edit.user_id)

            context = {
                'form': form,
                'article': article_to_edit,
                'article_pic': article_pic,
            }

        return render(request, 'articles/edit_article.html', context)

    raise PermissionDenied  # authorisation error


@require_GET
@login_required
def confirm_delete_article(request, pk):
    check_if_article_exists(pk)

    article = Articles.objects.get(pk=pk)
    article_pic = None
    if article.article_pic:
        article_pic = article.article_pic.url

    context = {
        'article': article,
        'article_pic': article_pic,
    }

    return render(request, 'articles/confirm_delete_article.html', context)


@require_GET
@login_required
def delete_article(request, pk):
    check_if_article_exists(pk)

    article = Articles.objects.get(pk=pk)
    pic_file_path = join(MEDIA_ROOT, str(article.article_pic))
    if request.user == article.user:
        article.delete()
        if isfile(pic_file_path):
            remove(pic_file_path)

        return redirect('your articles', request.user.id)

    raise PermissionDenied  # authorisation error


@require_GET
def show_about(request):
    return render(request, 'about.html')
