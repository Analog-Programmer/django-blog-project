from django.contrib import admin

# Register your models here.
from blog_project.articles.models import Articles

admin.site.register(Articles)
