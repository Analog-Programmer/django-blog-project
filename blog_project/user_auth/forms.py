from django import forms
# from django.contrib.auth.forms import UserCreationForm  # use this for better internal registration form
from django.contrib.auth.models import User

from blog_project.user_auth.models import UserExtend

# use this to attach attributes to widgets in ModelForms with functions:
# from blog_project.blog_project.utils.common_modelform_funcs import attach_html_attribs_to_modelform_widget_fields, remove_help_text_from_modelform_fields


# use this to attach attributes to widgets in ModelForms with class mixin:
class StylizeFormMixin(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs.update(
                {
                    'class': 'form-control  form-group',
                }
            )
            field.help_text = None


# class UserModelForm(StylizeForm, UserCreationForm):  # use this for better internal registration form!!!
class UserModelForm(StylizeFormMixin, forms.ModelForm):  # attaches attributes to widgets in ModelForms with class mixin
    # use this for attaching attributes to widgets in ModelForms with functions:
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     attach_html_attribs_to_modelform_widget_fields(self.visible_fields())
    #     remove_help_text_from_modelform_fields(self.visible_fields())

    class Meta:
        model = User
        # fields = ['username']  # use this on better internal registration form
        fields = ['username', 'password']


class UserExtendModelForm(StylizeFormMixin, forms.ModelForm):  # attaches attributes to widgets in ModelForms with class mixin
    class Meta:
        model = UserExtend
        exclude = ['user']


class UserEditModelForm(StylizeFormMixin, forms.ModelForm):  # attaches attributes to widgets in ModelForms with class mixin
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class UserLoginForm(forms.Form):  # using ModelForm brakes login() functionality due to User model "username" validation
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control form-group'
            }
        ),
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control form-group'
            }
        ),
    )
    fields = ['username', 'password']

    # for username and password validation on login:
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username not in User.objects.values_list('username', flat=True):
            self.add_error('username', 'Username does not exist.')  # renders error message in exact field of the form
            # raise forms.ValidationError('Username does not exist.')  # use to render error message on top of the form

        elif not User.objects.get(username=username).check_password(password):
            self.add_error('password', 'Wrong password.')  # renders error message in exact field of the form
            # raise forms.ValidationError('Wrong password.')  # use to render error message on top of the form
