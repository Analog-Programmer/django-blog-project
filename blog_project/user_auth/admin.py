from django.contrib import admin

# Register your models here.
# from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# from django.contrib.auth.models import User

from blog_project.user_auth.models import UserExtend

# does not work if new user with picture is created from admin panel,
# so F*** OFF goes to Django admin panel with User model extend to another model and 1-to-1 relation!

# creates inline admin descriptor for UserExtend model:
# class UserExtendInline(admin.StackedInline):
#     model = UserExtend
#     can_delete = False
#     verbose_name_plural = 'profile picture'
#
#
# # alter new User admin:
# class UserAdmin(BaseUserAdmin):
#     inlines = (UserExtendInline,)
#
#
# # register new User admin:
# admin.site.unregister(User)
# admin.site.register(User, UserAdmin)

admin.site.register(UserExtend)
