from django.urls import path

from blog_project.user_auth import views

urlpatterns = [
    path('show/<int:pk>', views.user_details, name='user details'),
    path('register/', views.register_user, name='register user'),
    path('login/', views.login_user, name='login user'),
    path('edit/<int:pk>', views.edit_user, name='edit user'),
    path('logout/', views.logout_user, name='logout user'),
]
