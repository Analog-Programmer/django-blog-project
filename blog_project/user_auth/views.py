from os import remove, listdir
from os.path import join, isfile, exists

from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, render

# Create your views here.
from django.views.decorators.http import require_GET

from blog_project.settings import MEDIA_ROOT
from blog_project.user_auth.forms import UserModelForm, UserExtendModelForm, UserLoginForm, UserEditModelForm
from blog_project.user_auth.models import UserExtend
from blog_project.utils.common_view_funcs import check_if_user_exists


def register_user(request):
    if request.user.is_anonymous:
        if request.method == 'GET':

            context = {
                'form': UserModelForm(),
            }

        else:
            form = UserModelForm(request.POST)

            if form.is_valid():
                user = form.save(commit=False)
                user.set_password(user.password)
                user.save()
                login(request, user)

                return redirect('user details', user.id)

            context = {
                'form': form,
            }

        return render(request, 'user_auth/register_user.html', context)

    raise PermissionDenied  # authorisation error


def login_user(request):
    if request.user.is_anonymous:
        if request.method == 'GET':
            context = {
                'form': UserLoginForm(),
            }

        else:
            form = UserLoginForm(request.POST)

            if form.is_valid():
                username = request.POST.get('username')
                password = request.POST.get('password')
                user = authenticate(username=username, password=password)

                if user:
                    if user.is_active:
                        login(request, user)

                    return redirect('index page')

            context = {
                'form': form,
            }

        return render(request, 'user_auth/login_user.html', context)

    raise PermissionDenied  # authorisation error


@require_GET
@login_required
def user_details(request, pk):
    check_if_user_exists(pk)

    user_profile = User.objects.get(pk=pk)
    user_profile_pic = None
    if UserExtend.objects.get(user_id=user_profile.id).profile_pic:
        user_profile_pic = UserExtend.objects.get(user_id=user_profile.id).profile_pic.url

    other_users_list = User.objects.exclude(id=user_profile.id)

    context = {
        'user_profile': user_profile,
        'user_profile_pic': user_profile_pic,
        'other_users_list': other_users_list.order_by('username'),  # alphabetically ordered by username
    }

    return render(request, 'user_auth/user_details.html', context)


@login_required
def edit_user(request, pk):
    check_if_user_exists(pk)

    user_to_edit = User.objects.get(pk=pk)
    user_profile_pic = None
    if UserExtend.objects.get(user_id=user_to_edit.id).profile_pic:
        user_profile_pic = UserExtend.objects.get(user_id=user_to_edit.id).profile_pic.url

    extend_user_to_edit = UserExtend.objects.get(user=pk)

    if request.user == user_to_edit:
        if request.method == 'GET':

            context = {
                'form': UserEditModelForm(instance=user_to_edit),
                'user_profile_pic': user_profile_pic,
                'extend_form': UserExtendModelForm(instance=extend_user_to_edit),
            }

        else:
            form = UserEditModelForm(request.POST, instance=user_to_edit)
            extend_form = UserExtendModelForm(request.POST, instance=extend_user_to_edit)

            if form.is_valid() and extend_form.is_valid():
                old_pic_file_path = join(MEDIA_ROOT, str(extend_user_to_edit.profile_pic))
                user_profile = form.save()
                extend_user_to_edit = extend_form.save(commit=False)
                extend_user_to_edit.user = user_profile

                # deletes old user profile pic if new image file is uploaded:
                if 'profile_pic' in request.FILES:
                    if isfile(old_pic_file_path):
                        remove(old_pic_file_path)
                    extend_user_to_edit.profile_pic = request.FILES['profile_pic']

                extend_user_to_edit.save()

                # deletes all old user profile pics if current pic is not set:
                if not extend_user_to_edit.profile_pic:
                    path_to_files = join(MEDIA_ROOT, 'images')
                    if exists(path_to_files):
                        [remove(join(path_to_files, file)) for file in listdir(path_to_files)
                         if exists(join(path_to_files, file))
                         and 'profile_pic_' + user_profile.username + '_' in join(path_to_files, file)]

                return redirect('user details', pk)

            context = {
                'form': form,
                'user_profile_pic': user_profile_pic,
                'extend_form': extend_form,
            }

        return render(request, 'user_auth/edit_user.html', context)

    raise PermissionDenied  # authorisation error


@require_GET
@login_required
def logout_user(request):
    logout(request)
    return redirect('index page')
