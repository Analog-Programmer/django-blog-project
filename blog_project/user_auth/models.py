from os.path import join

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

"""
Editable User model fields with "+":
- username - Required. 150 characters or fewer. Usernames may contain alphanumeric, _, @, +, . and - characters.
+ first_name - Optional (blank=True). 150 characters or fewer.
+ last_name - Optional (blank=True). 150 characters or fewer.
+ email - Optional (blank=True). Email address.
- password - Required. A hash of, and metadata about, the password. (Django doesn’t store the raw password.) Raw passwords can be arbitrarily long and can contain any character.
- groups - Many-to-many relationship to Group.
- user_permissions - Many-to-many relationship to Permission.
- is_staff - Boolean. Designates whether this user can access the admin site.
- is_active - Boolean. Designates whether this user account should be considered active.
- is_superuser - Boolean. Designates that this user has all permissions without explicitly assigning them.
- last_login - A datetime of the user’s last login.
- date_joined - A datetime designating when the account was created. Is set to the current date/time by default when the account is created.

Editable UserExtend model fields with "+":
- user - One-to-one relationship to User.
+ profile_pic - Optional (blank=True). An image file with profile picture.
"""


# creates path & filename for uploaded profile picture as "media/images/profile_pic_[username]_[filename]":
def make_image_upload_path(instance, filename):
    return join('images', 'profile_pic_' + instance.user.username + '_' + filename.lower())


class UserExtend(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_pic = models.ImageField(upload_to=make_image_upload_path, blank=True)

    # to create record for "user_pic" using signal from User model, if new user profile is created
    @staticmethod
    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserExtend.objects.create(user=instance)

    # to create record for "user_pic" using signal from User model, if user profile is saved
    @staticmethod
    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.userextend.save()

    # for admin panel functionality:
    def __str__(self):
        return f"{self.user.username} profile picture"
