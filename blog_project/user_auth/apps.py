from django.apps import AppConfig


class UserAuthConfig(AppConfig):
    name = 'blog_project.user_auth'
