# Generated by Django 3.1.3 on 2020-12-02 10:12

from django.db import migrations, models

from blog_project import user_auth


class Migration(migrations.Migration):

    dependencies = [
        ('user_auth', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userextend',
            name='profile_pic',
            field=models.ImageField(blank=True, upload_to=user_auth.models.make_image_upload_path),
        ),
    ]
