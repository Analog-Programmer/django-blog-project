# attaches "class" attributes to widgets in ModelForms:
def attach_html_attribs_to_modelform_widget_fields(all_visible_fields):
    for visible_field in all_visible_fields:
        visible_field.field.widget.attrs['class'] = 'form-control form-group'


# removes help text from ModelForms:
def remove_help_text_from_modelform_fields(all_visible_fields):
    for visible_field in all_visible_fields:
        visible_field.field.help_text = None


# makes "disabled" all visible fields in ModelForms
def disable_all_visible_in_modelform_fields(all_visible_fields):
    for visible_field in all_visible_fields:
        visible_field.field.widget.attrs['disabled'] = True
