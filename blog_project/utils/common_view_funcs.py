from django.contrib.auth.models import User
from django.http import Http404

# check if user with "id" = "pk" exists in User model:
from blog_project.article_comments.models import ArticleComments
from blog_project.articles.models import Articles


def check_if_user_exists(pk):
    if not User.objects.filter(pk=pk).exists():
        raise Http404
    return


# check if article with "id" = "pk" exists in Articles model:
def check_if_article_exists(pk):
    if not Articles.objects.filter(pk=pk).exists():
        raise Http404
    return


# check if comment with "id" = "pk" exists in ArticleComments model:
def check_if_article_comment_exists(pk):
    if not ArticleComments.objects.filter(pk=pk).exists():
        raise Http404
    return
