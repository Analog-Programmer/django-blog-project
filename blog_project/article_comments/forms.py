from django import forms

from blog_project.article_comments.models import ArticleComments


class StylizeFormMixin(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs.update(
                {
                    'class': 'form-control  form-group',
                }
            )


class CommentModelForm(StylizeFormMixin, forms.ModelForm):
    class Meta:
        model = ArticleComments
        fields = ['comment']
