from django.urls import path

from blog_project.article_comments import views

urlpatterns = [
    path('edit/<int:pk>', views.edit_comment, name='edit comment'),
    path('delete/<int:pk>', views.delete_comment, name='delete comment'),
    ]
