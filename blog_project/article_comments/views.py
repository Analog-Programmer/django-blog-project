from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect

# Create your views here.
from django.views.decorators.http import require_GET

from blog_project.article_comments.forms import CommentModelForm
from blog_project.article_comments.models import ArticleComments
from blog_project.utils.common_view_funcs import check_if_article_comment_exists


@login_required
def edit_comment(request, pk):
    check_if_article_comment_exists(pk)

    comment_to_edit = ArticleComments.objects.get(pk=pk)
    if request.user == comment_to_edit.user:
        if request.method == 'GET':

            context = {
                'form': CommentModelForm(instance=comment_to_edit),
                'comment': comment_to_edit,
            }

        else:
            comment_form = CommentModelForm(request.POST, instance=comment_to_edit)

            if comment_form.is_valid():
                comment_form.save()

                return redirect('show article', comment_to_edit.article_id, comment_to_edit.article.slug)

            context = {
                'form': comment_form,
                'comment': comment_to_edit,
            }

        return render(request, 'article_comments/edit_comment.html', context)

    raise PermissionDenied


@require_GET
@login_required
def delete_comment(request, pk):
    check_if_article_comment_exists(pk)

    comment = ArticleComments.objects.get(pk=pk)
    if request.user == comment.user:
        comment.delete()

        return redirect('show article', comment.article_id, comment.article.slug)

    raise PermissionDenied  # authorisation error
