from django.apps import AppConfig


class ArticleCommentsConfig(AppConfig):
    name = 'blog_project.article_comments'
