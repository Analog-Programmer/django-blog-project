from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from blog_project.articles.models import Articles


class ArticleComments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    article = models.ForeignKey(Articles, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    comment = models.TextField(blank=False)

    # for admin panel functionality:
    def __str__(self):
        article = self.article.title if len(self.article.title) <= 30 else self.article.title[:30] + '...'
        comment = self.comment if len(self.comment) <= 30 else self.comment[:30] + '...'
        return f'{self.user.username} commented "{article}" with "{comment}"'
