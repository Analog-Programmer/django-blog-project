from django.contrib import admin

# Register your models here.
from blog_project.article_comments.models import ArticleComments

admin.site.register(ArticleComments)
